# sense-dynamics

This repo contains the CFD part of the sense dynamics projects. This is based on open Softwares:

- python
- [`gmsh`](http://gmsh.info/)
- [`openFOAM`](https://www.openfoam.com/)

The following shows the workflow of using this repo. 

## Mesh Generation
We use `gmsh` for mesh generation and later convert them to `openFOAM`. Yet, there is a preparation step before having an optimized and ready-to-use mesh.

### preparation
1. Place your `.msh` file inside a copy of folder `mesh_generator/` (change the name according to the note below).
2. `gmshToFoam <YOUR_MESH_NAME>`
3. `python patchCorrector.py`
4. execute `Allmesh`

The first two steps are to convert the mesh to `openFoam`. the third step corrects the patch types. Finally, the `Allmesh` executable, performs a baffling and renumbering to make an optimum sliding mesh. After these steps `constant` folder is ready for running a simulation in `openFOAM`.

**Note**: Both `<YOUR_MESH_NAME>` as well as the name of must be in the following format: `<geometry>X<int>`. For instance:
	- `AX0`: **A**irfoil geometry, **0** level of mesh refinement
	- `CX2`: **C**ylinder geometry, **2** level of mesh refinement

## Making Case files
We are interested in 4 different cases:

1. time-independent input and stationary airfoil (steady-state)
2. time-independent input and rotating airfoil
3. time-dependent input and stationary airfoil 
4. time-dependent input and rotating airfoil

Although each case has its own case configuration, many of such settings are shared among the cases. Therefore, we use a utility class called `CaseMaker` to generate these four cases. This high-level class takes inputs (such as initial angle of attack or the rotation frequency and amplitude) and generates proper initial/boundary condition, simulation setting and job submission script. **This doesn't mean that the configuration is optimum! User must monitor the residuals and modify the settings if needed.**


### Limitation of `CaseMaker`
`CaseMaker` assumes that the mesh is already generated and prepared for usage. Moreover, the location of the prepared mesh must be given as an argument to this class. It retrieves the `constant` folder (by simply copying and pasting) from the root and places it in each case directory.


## Simulation
If all the settings are appropriate, one can use the `bash.run` scripts to run the submit the jobs to the cluster and start the simulations. The following order must be respected:

1. First, case (study) 1 must be run. The final solution (made by `reconstructPar -latestTime`), is needed both for analysis and as the initial condition to other cases.

2. `python setup_ic_from_steadystate.py` replaces the final solution of study 1 for the rest of the cases (note that the `0/` folders in studies 2 to 4 are empty before running this command). Also, it modifies the patches values for the time-dependent cases (using `timedep_input` file).

3. Each case now can be run independently.